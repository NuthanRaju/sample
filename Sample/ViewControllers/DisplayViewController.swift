//
//  DisplayViewController.swift
//  Sample
//
//  Created by Nuthan raju on 14/08/19.
//  Copyright © 2019 Sixpep Technovations Private Limited. All rights reserved.
//

import UIKit

class DisplayViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
}

extension DisplayViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if DataManager.shared.selectedFruit == .none {
            return 0
        }
        return DataManager.shared.quantity
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") {
            cell.textLabel!.text = "\(indexPath.row + 1) \(DataManager.shared.selectedFruit.rawValue)"
            return cell
        }
       return UITableViewCell()
    }
}

extension DisplayViewController: EventDelegate {
    func segmentControlChanged() {
        self.tableView.reloadData()
        if DataManager.shared.selectedFruit == .none {
            showAlert(title: "No Fruit was selected", message: "Please Select a Fruit")
        }
    }
    func showAlert(title: String,message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default) { (Action) in
            print("Ok Pressed")
        }
        alert.addAction(ok)
        self.present(alert,animated: true)
    }
}
