//
//  QuantityViewController.swift
//  Sample
//
//  Created by Nuthan raju on 14/08/19.
//  Copyright © 2019 Sixpep Technovations Private Limited. All rights reserved.
//

import UIKit

class QuantityViewController: UIViewController {

    @IBOutlet weak var quantityLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        //Default Minimum value
        DataManager.shared.quantity = 50
    }

    @IBAction func sliderDragged(_ sender: UISlider) {
        let quantity = Int(sender.value)
        quantityLabel.text = "\(quantity)"
        DataManager.shared.quantity = quantity
        
    }
}
