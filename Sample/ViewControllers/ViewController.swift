//
//  ViewController.swift
//  Sample
//
//  Created by Nuthan raju on 14/08/19.
//  Copyright © 2019 Sixpep Technovations Private Limited. All rights reserved.
//

import UIKit

enum Fruit: String {
    case apple = "Apple"
    case banana = "Banana"
    case kiwi = "Kiwi"
    case none = "none"
}

class DataManager {
    static let shared: DataManager = DataManager()
    var selectedFruit: Fruit = .banana
    var quantity: Int = 0
    private init() {
        
    }
}

protocol EventDelegate {
    func segmentControlChanged()
}

class ViewController: UIViewController {

    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    var delegate: EventDelegate?
    
    lazy var controllers: [UIViewController] = {
        if let storyBoard = self.storyboard {
            let fruitsVC: UIViewController = storyBoard.instantiateViewController(withIdentifier: String(describing: FruitsViewController.self))
            let quantityVC: UIViewController = storyBoard.instantiateViewController(withIdentifier: String(describing: QuantityViewController.self))
            let displayVC: UIViewController = storyBoard.instantiateViewController(withIdentifier: String(describing: DisplayViewController.self))
            self.delegate = (displayVC as? DisplayViewController)
            return [fruitsVC, quantityVC,displayVC]
        }
        return []
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addChildViewControllers()
        //Setting the Fruits View as the First default tab
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.sendActions(for: .valueChanged)
    }

    func addChildViewControllers() {
        for controller in controllers {
            controller.view.frame = holderView.bounds
            holderView.addSubview(controller.view)
            self.addChild(controller)
            controller.didMove(toParent: self)
        }
    }

    @IBAction func segmentedControlPressed(_ sender: UISegmentedControl) {
        showViewController(index: sender.selectedSegmentIndex)
        if sender.selectedSegmentIndex == 2 {
            delegate?.segmentControlChanged()
        }
    }
    
    func showViewController(index: Int) {
        for (i, controller) in controllers.enumerated() {
            if i == index {
                controller.view.isHidden = false
            } else {
                controller.view.isHidden = true
                
            }
        }
    }
}

