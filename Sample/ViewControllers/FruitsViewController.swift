//
//  FruitsViewController.swift
//  Sample
//
//  Created by Nuthan raju on 14/08/19.
//  Copyright © 2019 Sixpep Technovations Private Limited. All rights reserved.
//

import UIKit

class FruitsViewController: UIViewController {
    
    var radioGroupView: RadioGroupView = RadioGroupView(frame: .zero, titles: [Fruit.banana.rawValue, Fruit.apple.rawValue, Fruit.kiwi.rawValue])

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(radioGroupView)
        radioGroupView.translatesAutoresizingMaskIntoConstraints = false
        radioGroupView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        radioGroupView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 20).isActive = true
        radioGroupView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        radioGroupView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        radioGroupView.onSelect = { selectedIndex, isSelected in
            if !isSelected {
                DataManager.shared.selectedFruit = .none
                return
            }
            switch selectedIndex {
            case 0:
                DataManager.shared.selectedFruit = .banana
            case 1:
                DataManager.shared.selectedFruit = .apple
            case 2:
                DataManager.shared.selectedFruit = .kiwi
            default:()
            }
        }
    }

}
