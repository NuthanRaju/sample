//
//  RadioGroupView.swift
//  RadioGroupView
//
//  Created by Nuthan raju on 13/08/19.
//  Copyright © 2019 Google. All rights reserved.
//

import UIKit

class RadioGroupView: UIView {
    
    var titles: [String] = []
    lazy var stackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [])
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .vertical
        stack.alignment = .fill
        stack.distribution = .fillEqually
        return stack
    }()
    
    var selectedIndex = 0
    var onSelect: (Int, Bool) -> Void = {_,_ in}
    
     init(frame: CGRect, titles: [String]) {
        self.titles = titles
        super.init(frame: frame)
        addSubViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        addSubViews()
    }
    
    func addSubViews() {
        self.addSubview(stackView)
        stackView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        stackView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20).isActive = true
        stackView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        addElementsToStackView()
    }
    
    func addElementsToStackView() {
        for (index,title) in titles.enumerated() {
            stackView.addArrangedSubview(getViewForStack(title: title, index: index))
        }
        
    }
    
    func getViewForStack(title: String, index: Int) -> UIView {
        let view = UIView()
        let radioButton = UIButton(type: .custom)
        radioButton.translatesAutoresizingMaskIntoConstraints = false
        let imageName = (index == 0) ? "Selected" : "UnSelected"
        radioButton.setImage(UIImage(named: imageName), for: .normal)
        radioButton.tag = index
        radioButton.addTarget(self, action: #selector(radioButtonTapped(sender:)), for: .touchUpInside)
        view.addSubview(radioButton)
        
        let radioTitle = UILabel()
        radioTitle.translatesAutoresizingMaskIntoConstraints = false
        radioTitle.text = title
        view.addSubview(radioTitle)
        
        radioButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10).isActive = true
        radioButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 10).isActive = true
        radioButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
        radioButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        radioTitle.leftAnchor.constraint(equalTo: radioButton.rightAnchor, constant: 40).isActive = true
        radioTitle.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        radioTitle.topAnchor.constraint(equalTo: view.topAnchor, constant: 10).isActive = true
        radioTitle.heightAnchor.constraint(equalToConstant: 30).isActive = true
        return view
    }
    
    @objc func radioButtonTapped(sender: UIButton) {
        selectedIndex = sender.tag
        
        for subView in stackView.arrangedSubviews {
            for eachView in subView.subviews {
                if eachView is UIButton && eachView.tag == sender.tag {
                if let button = eachView as? UIButton {
                    if button.image(for: .selected) == UIImage(named: "Selected") {
                        button.setImage(UIImage(named: "UnSelected"), for: .normal)
                        onSelect(selectedIndex, false)
                    } else {
                        button.setImage(UIImage(named: "Selected"), for: .normal)
                        onSelect(selectedIndex, true)
                    }
                }
                } else {
                    if eachView is UIButton {
                        (eachView as! UIButton).setImage(UIImage(named: "UnSelected"), for: .normal)
                    }
                }
            }
        }
    }
}
